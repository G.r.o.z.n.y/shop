import {combineReducers, configureStore} from '@reduxjs/toolkit';
import authSlice from './reducers/authReducer';
import userSlice from './reducers/userReducer';
import authFormStateReducer from './reducers/authFormStateReducer';




const reducers = combineReducers({
    authFormStateReducer,
    userSlice,
    authSlice
});

export const store = configureStore({reducer: reducers});
