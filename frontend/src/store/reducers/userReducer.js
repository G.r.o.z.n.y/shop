import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
    name: 'userSlice',
    initialState: {
        user: {name: '', email: ''}
    },
    reducers: {
        userLogin(state, action) {
            state.user = action.payload
        },
        userLogout(state) {
            state.user = {name: '', email: ''}
        }
    }
})

export default userSlice.reducer;
export const {userLogin, userLogout} = userSlice.actions;