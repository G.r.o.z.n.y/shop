import { createAction, createReducer } from "@reduxjs/toolkit";

const initialState = {
    authFormState: 'login'
}

export const checkRegistration = createAction("CHECK_REGISTRATION");
export const checkLogin = createAction("CHECK_LOGIN");


export default createReducer (initialState, {
    [checkRegistration]: function (state) {
        state.authFormState = 'registration'
    },
    [checkLogin]: function (state) {
        state.authFormState = 'login'
    },
});
