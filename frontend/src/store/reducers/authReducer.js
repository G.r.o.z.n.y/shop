import { createSlice } from "@reduxjs/toolkit";

const authSlice = createSlice({
    name: 'authSlice',
    initialState: {
        isAuth: false
    },
    reducers: {
        setAuth(state) {
            state.isAuth = true
        },
        unsetAuth(state) {
            state.isAuth = false
        }
    }
})

export default authSlice.reducer;
export const {setAuth, unsetAuth} = authSlice.actions;