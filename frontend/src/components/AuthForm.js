import LoginForm from './LoginForm';
import RegistrationForm from './RegistrationForm';
import { useDispatch, useSelector } from 'react-redux';
import { checkRegistration, checkLogin } from '../store/reducers/authFormStateReducer';
import './components.css';

const AuthForm = () => {
    
    const dispatch = useDispatch();
    const authFormState = useSelector(state => state.authFormStateReducer.authFormState);
    const user = useSelector(state => state.userSlice.user);

    function changeAuthFormState(e){
        authFormState === 'login' ? dispatch(checkRegistration()) : dispatch(checkLogin());
        document.getElementById('login-button').className = document.getElementById('login-button').className === "nav-link" ? "nav-link active" : "nav-link";
        document.getElementById('registration-button').className = document.getElementById('login-button').className === "nav-link" ? "nav-link active" : "nav-link";
    };

    function Form(props) {
        return (props.formType === 'login') ? <LoginForm/> : <RegistrationForm/>;
    };

    return (
        <div className="loginForm authForm">
           <div className="mb-3">
                <h1>Добро пожаловать в Shop!</h1>
            </div>
            <div className="mb-3">
                <ul className="nav nav-tabs nav-justified" role="tablist" id="auth-form-cheker">
                    <li className="nav-item">
                        <button className="nav-link active" id="login-button" data-toggle="tab"  onClick={changeAuthFormState} role="tab">Вход</button>
                    </li>
                    <li className="nav-item">
                        <button className="nav-link" id="registration-button" data-toggle="tab"  onClick={changeAuthFormState} role="tab">Регистрация</button>
                    </li>
                </ul>
            </div>
           <Form formType={authFormState}/>
           <h3>Пользователь - {user?.email}</h3>
        </div>
    )   
}

export default AuthForm;


