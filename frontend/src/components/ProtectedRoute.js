import React from 'react';
import { Route } from 'react-router-dom';

const ProtectedRoute = ({ component: Component, user, ...rest }) => {
  return (
    <Route {...rest} render={
      props => {
          if (user) {
            return <Component {...rest} {...props} />
          } else {
            return   <h1>Страница только для авторизованных</h1>
          }
      } 
    } />
  )
}

export default ProtectedRoute;
