import React, {useState} from 'react';
import './components.css';
import AuthService from '../services/AuthService';
import { useDispatch } from 'react-redux';
import { userLogin } from '../store/reducers/userReducer';
import { setAuth } from '../store/reducers/authReducer';



const LoginForm = () => {
    const dispatch = useDispatch();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const login = async function(email, password) {
        try {
          const response = await AuthService.login(email, password);
          localStorage.setItem('token', response.data.accessToken);
          dispatch(userLogin(response.data.user));
          dispatch(setAuth());
        } catch (e) {
          console.log(e.response?.data?.message);
        }
      }; 

      const handleSubmit = (e) => {
        e.preventDefault(); 
        login(email, password);
      }

    return (
        <form className="loginForm">
            <div className="mb-3">
                <input 
                    onChange = {e => setEmail(e.target.value)}
                    value={email}
                    type="email" 
                    className="form-control" 
                    id="loginForm-login" 
                    placeholder="Введите email"
                    />
                <div id="emailHelp" className="form-text"></div>
            </div>
            <div className="mb-3">
                <input 
                    type="password" 
                    className="form-control" 
                    id="loginForm-password"
                    onChange = {e => setPassword(e.target.value)}
                    value={password}
                    placeholder="Введите пароль"
                    />
            </div>
            <button onClick={handleSubmit} className="btn btn-primary">Войти</button>            
        </form>
      )
}

export default LoginForm;


