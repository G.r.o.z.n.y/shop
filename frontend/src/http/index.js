import axios from 'axios';

export const API_URL = 'http://localhost:5000/API';

const $api = axios.create({
    withCredentials: true,
    baseURL: API_URL
});

$api.interceptors.request.use((config => {
    config.headers.Authorization = `Bearer ${localStorage.getItem('token')}`;
    console.log('interceptors для добавления токена в request', config?.headers);
    return config;
}));

$api.interceptors.request.use((config) => {
    console.log('еще один интерцептор', config);
    return config;
}, async (error) => { 
    const originalRequest = error.config;
    if (error.responseType.status === 401 && error.config && !error.config._isRetry) {
        originalRequest._isRetry = true;
        
        try {
            const response = await axios.get(`${API_URL}/refresh`, {withCredentials: true});
            console.log('response интерцептор 2', response);
            localStorage.setItem('token', response.data.accessToken);
            return $api.request(originalRequest);
        } catch (e) {
            console.log(' Не авторизован!!! ');
        }
    }
    throw error;

});



export default $api;