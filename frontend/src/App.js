import './App.css';
import Cart from './components/Cart';
import Account from './components/Account';
import Products from './components/Products';
import Product from './components/Product';
import Footer from './footer';
import Navbar from './components/Navbar';
import AuthForm from './components/AuthForm';
import {useDispatch, useSelector} from 'react-redux';
import axios from 'axios';
import {API_URL} from './http/index';
import { useEffect } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import AuthService from './services/AuthService';
import userSlice, { userLogin, userLogout } from './store/reducers/userReducer';
import { setAuth, unsetAuth } from './store/reducers/authReducer';
import ProtectedRoute from './components/ProtectedRoute'


function App() {

  const dispatch = useDispatch();


  const logout = async function() {
    console.log('logout-');
    try {
      const response = await AuthService.logout();
      localStorage.removeItem('token');
      dispatch(userLogout());
      dispatch(unsetAuth());
    } catch (e) {
      console.log('логаут не работает');
      console.log(e);
    }
  }; 

  const checkAuth = async function() {
    try {
      const response = await axios.get(`${API_URL}/refresh`, {withCredentials: true});
      localStorage.setItem('token', response.data.accessToken);
      dispatch(userLogin(response.data.user));
      dispatch(setAuth());
      //здесь надо юзера в стейт - редьюсер проверить 
    } catch (e) {
      console.log(e);
    }
  }; 

  useEffect(() => {    
    if (localStorage.getItem('token')) {
       console.log('В localStorage есть токен', localStorage.getItem('token'));
       console.log('useEffect');
       checkAuth();
    }
  },[]);


  const isAuth = useSelector(state => state.authSlice.isAuth);
  const user = useSelector(state => state.userSlice.user);


  if (!isAuth) {
    return <AuthForm/>
  } else {
    return (
      <BrowserRouter>
        <Navbar />
          <div className="container">
            <button onClick={logout}>logout!</button>
            <Switch>
              <ProtectedRoute exact path='/cart' component={Cart} user={user}/>
              <ProtectedRoute exact path='/' component={Product} user={user}/>
              <ProtectedRoute exact path='/account' component={Account} user={user}/>
              <ProtectedRoute exact path='/products' component={Products} user={user}/>
            </Switch>
          </div>
          <Footer/>
      </BrowserRouter>
      
    ); 
  };

}

export default App;
