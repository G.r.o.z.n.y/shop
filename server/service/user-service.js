const userModel = require("../models/user-model");
const bcrypt = require('bcrypt');
const uuid = require('uuid');
const tokenService = require('./token-service');
const mailService = require('./mail-service');
const UserDto = require('../dtos/user-dto')
const ApiError = require('../exceptions/api-error');

class UserService {
    async registration(email, password) {
        try {
            const candidate = await userModel.findOne({email});
            if (candidate) {
                throw ApiError.BadRequest(`Пользователь с почтовым адресом ${email} уже существует!`)
            }
            const hashPassword = await bcrypt.hash(password, 3);
            const activationLink = uuid.v4();
            const user = await userModel.create({email, password:hashPassword, activationLink});
            await mailService.sendActivationMail(email, `${process.env.API_URL}/API/activate/${activationLink}`);
    
            const userDto = new UserDto(user);
            const tokens = tokenService.generateTokens({...userDto});
            await tokenService.saveToken(userDto.id, tokens.refreshToken);
            return {...tokens, user: userDto}
        } catch( e) {
            console.log('e',e);
        }
    }

    async activate(activationLink) {
        const user = await userModel.findOne({activationLink});
        if (!user) {
            throw ApiError.BadRequest('Некорректная ссылка активации!'); 
        }
        user.isActivated = true;
        await user.save();
    }

    async login(email, password) {
        const user = await userModel.findOne({email});
        if (!user) {
            throw ApiError.BadRequest(`Пользователь с почтовым адресом ${email} не существует!`)
        }
        const isPassEquals = await bcrypt.compare(password, user.password);
        if (!isPassEquals)  {
            throw ApiError.BadRequest(`Неверный пароль!`)
        }
        const userDto = new UserDto(user);
        const tokens = tokenService.generateTokens({...userDto});
        await tokenService.saveToken(userDto.id, tokens.refreshToken);
        console.log('success login', userDto.id);
        return {...tokens, user: userDto}
    }

    async logout(refreshToken) {
        const token = await tokenService.removeToken(refreshToken);
        return token;
    }

    async refresh(refreshToken) {
         if (!refreshToken) {
             console.log('async refresh(refreshToken)');
             throw ApiError.UnauthorizedError();
         }
         console.log('refreshToken', refreshToken);

         const userData = tokenService.validateRefreshToken(refreshToken);
         console.log('userData', userData);
         const tokenFromDb = await tokenService.findToken(refreshToken);
         console.log('tokenFromDb', tokenFromDb);

         if (!userData || !tokenFromDb) {
             console.log('async refresh(refreshToken)222');
             throw ApiError.UnauthorizedError();
         }

         const user = await userModel.findById(userData.id);
         if (user) {
             console.log('user', user);
             const userDto = new UserDto(user);
             const tokens = tokenService.generateTokens({...userDto});
             await tokenService.saveToken(userDto.id, tokens.refreshToken);
             return {...tokens, user: userDto}
         }
    }

    async getAllUsers() {
        const users = await userModel.find();
        return users;
    }

}

module.exports = new UserService();